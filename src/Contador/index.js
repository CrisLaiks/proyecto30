import React from "react";
import './contador.css';

function Contador({total, completed}){

    return(
       <h2>
        Tareas completadas {completed} de {total}
       </h2>
    )
}
export{Contador};