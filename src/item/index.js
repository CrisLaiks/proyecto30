import React from "react";
import './Item.css';

function Item(props){

    return(
      <div className="card">
      <div className="box">
        <div className="content">
          <h2>{props.numero}</h2>
          <p className={props.completed ? 'completado':''}>{props.text}</p>
          <button
          onClick={props.onComplete}
          >Marcar Completada</button>
          <button
          onClick={props.onEliminar}
          >
            Eliminar
            </button>
        </div>
      </div>
    </div>
    )
}
export{Item};