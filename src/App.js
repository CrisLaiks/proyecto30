import React from 'react';
import { Titulo  } from './Titulo/titulo';
import { Buscador } from './buscador/index';
import { Item } from './item/index';
import {Lista} from './Lista/index';
import {Boton} from './Boton/index';
import { Contador } from './Contador/index';
import './App.css';


/*
const tareasDefault = [
  {text:'Crear los componentes de la app',completed:false},
  {text:'Crear buscador',completed:false},
  {text:'Crear marcar tarea',completed:false},
  {text:'Crear eliminar tarea',completed:false},
  {text:'VIVA MEXICO >:V',completed:false},
  {text:'Mexico es mi pais :)',completed:false},
]*/
function App() {
//local storage
const localStorageTareas = localStorage.getItem('Tareas_v1')
let parsedTareas

if (!localStorageTareas) {
  localStorage.setItem('Tareas_v1', JSON.stringify([]))
}else{
  parsedTareas = JSON.parse(localStorageTareas)
};

const guardarTareas = (newTarea) => {
  const stringTareas = JSON.stringify(newTarea)
  localStorage.setItem('Tareas_v1', stringTareas)
  setTareas(newTarea)
}
  const [tareas, setTareas] = React.useState(parsedTareas);
  const completdTareas = tareas.filter(tarea=>!! tarea.completed).length
  const totalTareas = tareas.length
  //completar
  const completarTarea = (text)=>{
    const TareaIndex = tareas.findIndex(tarea => tarea.text === text)
    const nuevasTareas = [...tareas]
    nuevasTareas[TareaIndex].completed = true
    guardarTareas(nuevasTareas)
  }
  //eliminar
  const eliminarTarea = (text) =>{
    const TareaIndex = tareas.findIndex(tarea => tarea.text === text);
    const nuevasTareas = [...tareas];
    nuevasTareas.splice(TareaIndex, 1);
    guardarTareas(nuevasTareas);
  };
  //Buscador
  const [buscarValor,setBuscarValor] = React.useState('');
  let buscarNotas = tareas
  if(!buscarValor.length >= 1){
    buscarNotas = tareas
  } else{
    buscarNotas = tareas.filter(tarea=>{
      const tareaText = tarea.text.toLowerCase()
      const buscarText = buscarValor.toLowerCase()
      return tareaText.includes(buscarText)
    })
  }

  return (
    <React.Fragment>
    <div className="App">
      <Titulo/>
      <Contador
      total={totalTareas}
      completed={completdTareas}
      />
      <Buscador
      buscarValor={buscarValor}
      setBuscarValor={setBuscarValor}
      />
      <Lista>
        {buscarNotas.map((tarea, index)=>(
          <Item
          text={tarea.text}
          numero={'0'+ (index+1)}
          key={tarea.text}
          completed = {tarea.completed}
          onComplete={()=> completarTarea(tarea.text)}
          onEliminar={()=> eliminarTarea(tarea.text)}
          />
      ))}
      </Lista>
      <Boton/>
    </div>
    </React.Fragment>
  );
}

if(navigator.serviceWorker){
  navigator.serviceWorker.register('./public/sw.js')
  .then(reg=>{
        Notification.requestPermission().then(result =>{
            console.log(result)
            reg.showNotification('Hola mundo')
        })
    })

}
/*fetch('https://reqres.in/api/users')
.then(response => response.text())
.then(console.log);*/
//Mensaje del cache
/*
if(window.caches){
  caches.open('cache1')
  caches.open('cache2')

  //verificando si existe
  caches.has('cache2').then(console.log)

  /*caches.open('cache4').then(cache =>{
      cache.add('/index.html')
  })
  caches.open('cache5').then(cache =>{
      cache.add('../pages/offline.html')
  })
}*/

export default App;