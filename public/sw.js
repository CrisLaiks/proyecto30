//Ciclo de vida
//Install
self.addEventListener('install', event =>{
    console.log('SW: INstalando SW')
    const instalacion = new Promise ((resolve,rejet)=>{
        setTimeout(()=>{
            console.log('Instalaciones terminadas')
            resolve()
        },1000)
    })
    event.waitUntil(instalacion)
})

//Activate
self.addEventListener('activate', event => {
    console.log('SW: Activo y listo para tomar el control')
})

//Bloquear conexion por estabilidad de internet
/*self.addEventListener('fetch', event =>{
    console.log('SW:', event.request.url)
    if(event.request.url.includes('api/users')){
        const resp = new Response(`
        {ok:false, mensaje:no es posible conectar a la pagina}
        `)
        event.respondWith(resp)
    }
})*/
//sync
/*self.addEventListener('sync', event =>{
    console.log('Tenemos conexion xD')
    console.log(event)
})*/
self.addEventListener('fetch', event =>{
    const offline = new Response(`
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notas chidas</title>
</head>
<body>
    <h1>A ocurrido un error</h1>
    <h5>Intentelo mas tarde</h5>
</body>
</html>
    `,{
        headers:{
            'Content-Type':'text/html'
        }
    })

    const resp = fetch(event.request)
    .catch(() => offline)
    event.respondWith(resp)

})
/*self.addEventListenet('fetch', event =>{

    const offline = new Response(`
    Bienvenido a mi web, para ser utilizada
    es necesario tener una conexion a internet.
    `)

    const resp = fetch(event.request)
    .catch(() => offline)

    event.respondWith(resp)
})*/
//estrategias de cache
/*
self.addEventListener('install', event =>{
    const cacheProme = caches.open('estatico-01')
    .then(cache =>{
        return cache.addAll([
            './',
            'index.html',
            'offline.html',
            'services.js'
        ])
    })
    event.waitUntil(cacheProme)
})
//Solo venga del cache
self.addEventListener('fetch', event =>{
    event.respondWith(caches.match(event.request))
})

//Cache with newtar fallback
/*
self.addEventListener('fetch', event =>{
    const respuesta = caches.match(event.request)
    .then(res => {
        if(res) return res
        //No existen
        console.log('No existen', event.request)
        return fetch(event.request).then(newResp=>{
            caches.open('dinamico-01')
            .then(cache =>{
                cache.put(event.request, newResp)
            })
            return newResp.clone()
        })
    })
    event.respondWith(respuesta)
})*/

