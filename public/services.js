if(navigator.serviceWorker){
    navigator.serviceWorker.register('./sw.js')
    .then(reg=>{
        Notification.requestPermission().then(result =>{
            console.log(result)
            reg.showNotification('Hola bienvenido a notas shidas')
        })
    })
}
/*fetch('https://reqres.in/api/users')
.then(response => response.text())
.then(console.log);*/

//Mensaje del cache
if(window.caches){

    caches.open('cache4').then(cache =>{
        cache.add('./index.html')
    })
    caches.open('cache5').then(cache =>{
        cache.add('./offline.html')
    })
}